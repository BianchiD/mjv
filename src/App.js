import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import logoSpotify from "./images/spotify.png";
import queryString from "query-string";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pesquisaArtista, buscaArtista } from './store/artistaAction'
import RenderArtista from './RenderArtista'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { user: {}, accessToken: '' }
  }

  componentDidMount() {
    let parsed = queryString.parse(window.location.search)
    let accessToken = parsed.access_token
    this.setState({ accessToken })
    fetch('https://api.spotify.com/v1/me', {
      headers: {
        'Authorization': 'Bearer ' + accessToken
      }
    }).then(response => response.json())
      .then(data => this.setState({ user: { name: data.display_name } }))
  }

  render() {
    return (
      <div>
        <header className="CenterImg">
          <img className="Img" src={logoSpotify} alt="Spotify"></img>
        </header>
        <br />
        <div className="container">
          {
            !this.state.accessToken ?
              <div className="row">
                <div className="col-sm-5 Center">
                  <button className="btn btn-primary btn-lg" type="button" onClick={() => window.location.href = 'http://localhost:8888/login'}>Login</button>
                </div>
              </div>
              :
              <div className="row">
                <div className="col-sm-5 Center">
                  <div className="input-group mb-3">
                    <input type="text" className="form-control" autoFocus placeholder="O que procura?" value={this.props.pesquisa} onChange={this.props.buscaArtista} />
                    <div className="input-group-append">
                      <button className="btn btn-outline-secondary" type="button" id="button-addon2" onClick={() => this.props.pesquisaArtista(this.props.pesquisa, this.state.accessToken)}>
                        <i className="fa fa-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
          }

          <br />
          <br />
          <br />
          <div className="row">
            <RenderArtista></RenderArtista>
          </div>
        </div>
      </div>

    );
  }
}

const mapStateToProps = state => ({ artista: state.artista, pesquisa: state.artista.pesquisa })
const mapDispatchToProps = dispatch => bindActionCreators({ pesquisaArtista, buscaArtista }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(App)