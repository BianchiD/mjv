const URL = 'https://api.spotify.com/v1/search?'

export const pesquisaArtista = (searchText, accessToken) => {
    console.log(searchText)
    return dispatch => {
        fetch(`${URL}q=${searchText}&type=artist`, {
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
        
            .then(data => dispatch({ type: 'LIST_ARTISTS', payload: data })
        )
    }
}
export const buscaArtista = (e) => ({
    type: 'PESQUISA_ARTISTA', 
    payload: e.target.value
})