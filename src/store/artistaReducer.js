const INITIAL_STATE = {
    pesquisa: '',
    artista: [],
}

export default (state = INITIAL_STATE, actions) => {

    switch (actions.type) {
        case 'LIST_ARTISTS':
            return {...state, artista: actions.payload.artists.items }
        case 'PESQUISA_ARTISTA':
            return { ...state, pesquisa: actions.payload}
        default:
            return state
    }
}