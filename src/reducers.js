import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

import artista from './store/artistaReducer'

const rootReducer = combineReducers({
    artista:artista
})

export default rootReducer
