import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { pesquisaArtista } from './store/artistaAction'
import banda from './images/banda.jpg';

class RenderArtista extends Component {
  listaArtista(artista) {

    return artista.map(item => (
      <div key={item.id}>
        <div className="card " style={{ width: '14rem' }}>
          <img src={banda} class="card-img-top" alt="Image"></img>
          <div className="card-body">
            <h5 className="card-title" > {item.name}</h5>
          </div>
        </div>
      </div>
    ))
  }

  render() {
    return (
      this.listaArtista(this.props.artista.artista)
    );
  }
}

const mapStateToProps = state => ({ artista: state.artista })
const mapDispatchToProps = dispatch => bindActionCreators({ pesquisaArtista }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(RenderArtista)