﻿
# MJV Front-end
Projeto front-end da MJV consumindo a API do Spotify

## Começando

### Pré-requisito
Node.Js
React

### Características
Realiza login para pesquisar artistas no Spotify

### Instalando e usando:
Executar o prompt de comando e navegar até o diretório:
\Mjv\Servidor-Spotify
Executar o comando
```
set SPOTIFY_CLIENT_ID=ID do usuário do Spotify Developer
set SPOTIFY_CLIENT_SECRET=Senha do usuário do Spotify Developer
```
Executar o servidor local do spotify
```
npm start
```
Navegar até o diretório \Mjv
e Iniciar o react
```
npm start
```
Abrir o navegar e acessar o endereço localhost:3000
Clicar em Login
Digitar o nome do artista na barra de pesquisa
Clicar na lupa ao lado da barra de pesquisa
Os resultado serão exibidos logo abaixo da barra de pesquisa
```
 https://drive.google.com/file/d/1K9ScLmihp_ND8z7upGfKaWY-3weHvxmp/view?usp=sharing
```

## Authors

-   **Diogo Bianchi**  

